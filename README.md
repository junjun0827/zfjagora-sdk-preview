# ZFJAgoraSDKPreview

#### 介绍
声网Preview版本的SDK的demo

#### SDK下载地址

https://docs.agora.io/cn/live-streaming-4.x-preview/downloads?platform=iOS

或者

pod 'AgoraRtcEngine_iOS_Preview'
