//
//  AGBaseView.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "AGBaseView.h"

@implementation AGBaseView

- (instancetype)init{
    if(self == [super init]){
        [self uiConfig];
    }
    return self;
}

- (void)uiConfig{
    NSLog(@"AGBaseView");
}

@end
