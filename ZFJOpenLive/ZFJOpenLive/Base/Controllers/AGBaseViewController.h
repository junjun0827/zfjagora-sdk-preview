//
//  AGBaseViewController.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AGBaseViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
