//
//  NSObject+AGHelp.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (AGHelp)

///
+ (NSString *)name;
///
+ (NSString *)className;

@end

NS_ASSUME_NONNULL_END
