//
//  MBProgressHUD+MBP.h
//  same
//
//  Created by ZhangFujie on 2021/12/10.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (MBP)

/// 在指定view上显示渐隐提示框
+ (void)showMessage:(NSString *)message toView:(UIView *)view;
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;
+ (void)showError:(NSString *)error toView:(UIView *)view;
+ (void)showWarning:(NSString *)warning toView:(UIView *)view;
+ (void)showMessageWithImageName:(NSString *)imageName message:(NSString *)message toView:(UIView *)view;

/// 在window上显示渐隐提示框
+ (void)showMessage:(NSString *)message;
+ (void)showSuccess:(NSString *)success;
+ (void)showError:(NSString *)error;
+ (void)showWarning:(NSString *)warning;
+ (void)showMessageWithImageName:(NSString *)imageName message:(NSString *)message;

/// 在window上显示loading视图
+ (void)showLoading;
+ (void)showLoading:(NSString *)message;

/// 在window上隐藏loading视图
+ (void)hideLoading;
+ (void)hideLoading:(NSString *)message;
+ (void)hideLoadingError:(NSString *)message;
+ (void)hideLoadingSuccess:(NSString *)message;
+ (void)hideLoadingWarning:(NSString *)message;

///  显示进度条
+ (void)showProgress:(NSString *)title;
/// 更新进度条的值
+ (void)updateProgress:(CGFloat)progress;
/// 隐藏进度条
+ (void)hideProgress:(BOOL)animated;
+ (void)hideProgress:(BOOL)animated completed:(void(^)(void))completed;

/// 是否正在显示
+ (BOOL)isShowing;
+ (MBProgressHUD *)isShowingToView:(UIView *)view;

@end
