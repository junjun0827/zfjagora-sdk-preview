//
//  MBProgressHUD+MBP.m
//  same
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "MBProgressHUD+MBP.h"

/// 统一的显示时长
#define kHudShowTime 1.5f
/// 统一的字体
#define kHudFont [UIFont systemFontOfSize:14]

@implementation MBProgressHUD (MBP)

/// 在指定view上显示
+ (void)showMessage:(NSString *)message toView:(UIView *)view{
    [self show:message icon:nil view:view];
}

+ (void)showSuccess:(NSString *)success toView:(UIView *)view{
    [self show:success icon:@"mb_hud_success.png" view:view];
}

+ (void)showError:(NSString *)error toView:(UIView *)view{
    [self show:error icon:@"mb_hud_error.png" view:view];
}

+ (void)showWarning:(NSString *)warning toView:(UIView *)view{
    [self show:warning icon:@"mb_hud_warn.png" view:view];
}

/// 显示自定义图片信息
+ (void)showMessageWithImageName:(NSString *)imageName message:(NSString *)message toView:(UIView *)view{
    [self show:message icon:imageName view:view];
}

/// 在window上显示
+ (void)showMessage:(NSString *)message{
    [self show:message icon:nil view:nil];
}

+ (void)showSuccess:(NSString *)success{
    [self show:success icon:@"mb_hud_success.png" view:nil];
}

+ (void)showError:(NSString *)error{
    [self show:error icon:@"mb_hud_error.png" view:nil];
}

+ (void)showWarning:(NSString *)warning{
    [self show:warning icon:@"mb_hud_warn.png" view:nil];
}

+ (void)showMessageWithImageName:(NSString *)imageName message:(NSString *)message{
    [self showMessageWithImageName:imageName message:message toView:nil];
}

/// 显示带图片或者不带图片的信息
+ (void)show:(NSString *)text icon:(NSString *)iconName view:(UIView *)view{
    if (text == nil || text.length == 0) return;
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.detailsLabel.text = text;
    hud.detailsLabel.font = kHudFont;
    if (iconName == nil) {
        hud.mode = MBProgressHUDModeText;
    }else{
        UIImage *img = [UIImage imageNamed:iconName];
        hud.customView = [[UIImageView alloc] initWithImage:img];
        hud.mode = MBProgressHUDModeCustomView;
    }
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:kHudShowTime];
}

+ (void)showLoading{
    [MBProgressHUD showLoading:nil];
}

+ (void)showLoading:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        hud.detailsLabel.text = ag_stringValue(message);
        hud.detailsLabel.font = kHudFont;
        hud.animationType = MBProgressHUDAnimationZoom;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.removeFromSuperViewOnHide = YES;
    });
}

+ (void)hideLoading{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    });
}

+ (void)hideLoading:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self show:message icon:nil view:[UIApplication sharedApplication].keyWindow];
        });
    });
}

+ (void)hideLoadingError:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self show:message icon:@"mb_hud_error.png" view:[UIApplication sharedApplication].keyWindow];
        });
    });
}

+ (void)hideLoadingSuccess:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self show:message icon:@"mb_hud_success.png" view:[UIApplication sharedApplication].keyWindow];
        });
    });
}

+ (void)hideLoadingWarning:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self show:message icon:@"mb_hud_warn.png" view:[UIApplication sharedApplication].keyWindow];
        });
    });
}

///  显示进度条
+ (void)showProgress:(NSString *)title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [[UIApplication sharedApplication].delegate window];
        MBProgressHUD *hud = [window viewWithTag:1024];
        if(hud == nil){
            hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
        }
        hud.detailsLabel.text = ag_stringValue(title);
        hud.detailsLabel.font = kHudFont;
        hud.animationType = MBProgressHUDAnimationZoom;
        hud.mode = MBProgressHUDModeAnnularDeterminate;
        hud.removeFromSuperViewOnHide = YES;
        hud.tag = 1024;
        [hud showAnimated:YES];
    });
}

/// 更新进度条的值
+ (void)updateProgress:(CGFloat)progress{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [[UIApplication sharedApplication].delegate window];
        MBProgressHUD *hud = [window viewWithTag:1024];
        if(progress >=0 && progress < 1.0){
            hud.progress = progress;
        }else{
            [hud hideAnimated:YES];
        }
    });
}

/// 隐藏进度条
+ (void)hideProgress:(BOOL)animated completed:(void(^)(void))completed{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [[UIApplication sharedApplication].delegate window];
        MBProgressHUD *hud = [window viewWithTag:1024];
        [hud hideAnimated:animated];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (completed) {
                completed();
            }
        });
    });
}

/// 隐藏进度条
+ (void)hideProgress:(BOOL)animated{
    [[self class] hideProgress:animated completed:nil];
}

/// 是否正在显示
+ (BOOL)isShowing{
    return [self isShowingToView:[UIApplication sharedApplication].keyWindow] != nil ? YES : NO;
}

+ (MBProgressHUD *)isShowingToView:(UIView *)view{
    for (UIView *obj in view.subviews) {
        if([obj isKindOfClass:[MBProgressHUD class]]){
            return (MBProgressHUD *)obj;
        }
    }
    return nil;
}


/*
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         if (completed) {
             completed();
         }
     });*/

@end
