//
//  NSObject+AGHelp.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import "NSObject+AGHelp.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation NSObject (AGHelp)

+ (NSString *)name {
    return @(class_getName(self));
}

+ (NSString *)className {
    return [self name];
}

@end
