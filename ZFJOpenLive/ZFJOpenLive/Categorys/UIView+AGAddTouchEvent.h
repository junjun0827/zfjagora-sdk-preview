//
//  UIView+AGAddTouchEvent.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import <UIKit/UIKit.h>

typedef void(^AGViewTouchUpInsideBlock)(UIView *sender);

@interface UIView (AGAddTouchEvent)

/// 点击事件
@property (nonatomic,  copy) AGViewTouchUpInsideBlock touchUpInsideBlock;

@end
