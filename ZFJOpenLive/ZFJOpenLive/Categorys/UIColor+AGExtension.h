//
//  UIColor+AGExtension.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (AGExtension)

+ (UIColor *)ag_colorWithHexStr:(NSString *)string;
///
+ (UIColor *)ag_colorWithHexStr:(NSString *)string alpha:(CGFloat)alpha;
///
+ (UIColor *)ag_colorWithR:(CGFloat)red g:(CGFloat)green b:(CGFloat)blue a:(CGFloat)alpha;
///
+ (NSString *)ag_hexStringWithColor:(UIColor *)color;
///
- (BOOL)ag_colorIsWhite;

@end

NS_ASSUME_NONNULL_END
