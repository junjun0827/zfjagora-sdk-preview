//
//  UIView+AGAddTouchEvent.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "UIView+AGAddTouchEvent.h"
#import <objc/message.h>

static id kAddTouchEventKey;

@implementation UIView (AGAddTouchEvent)

- (void)setTouchUpInsideBlock:(AGViewTouchUpInsideBlock)touchUpInsideBlock{
    objc_setAssociatedObject(self, &kAddTouchEventKey, touchUpInsideBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchUpInsideAction:)];
    [self addGestureRecognizer:tap];
}

- (AGViewTouchUpInsideBlock)touchUpInsideBlock{
    return objc_getAssociatedObject(self, &kAddTouchEventKey);
}

- (void)touchUpInsideAction:(UITapGestureRecognizer *)sender{
    if(self.touchUpInsideBlock){
        self.touchUpInsideBlock(self);
    }
}

@end
