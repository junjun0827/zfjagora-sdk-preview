//
//  AGCommon.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#ifndef AGCommon_h
#define AGCommon_h

/// 弱引用
#define WEAKBLOCK __weak typeof(self) weakSelf = self;
/**
 *  重写NSLog,打印日志，当前行
 */
#if DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"\n方法名:%s 行数:%d 内容:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif

/// 判断是不是空
static inline BOOL ag_emptyString(NSString *str){
    if (str == nil){
        return YES;
    }
    if (!str) {
        return YES;
    }
    if (![str isKindOfClass:[NSString class]]) {
        return YES;
    }
    if (str.length == 0) {
        return YES;
    }
    return NO;
}

/// 强制转换字符串
static inline NSString *ag_stringValue(id str){
    if (!str || [str isKindOfClass:[NSNull class]]) {
        return @"";
    }
    if (![str isKindOfClass:[NSString class]]) {
        return [NSString stringWithFormat:@"%@", str];
    }
    return str;
}

/// 获取随机颜色
static inline UIColor *ag_randomColor(){
    NSInteger aRedValue = arc4random() % 255;
    NSInteger aGreenValue = arc4random() % 255;
    NSInteger aBlueValue = arc4random() % 255;
    UIColor *randColor = [UIColor colorWithRed:aRedValue /255.0f green:aGreenValue /255.0f blue:aBlueValue /255.0f alpha:1.0];
    return randColor;
}

static inline UIColor *ag_randomColorAlpha(CGFloat alpha){
    NSInteger aRedValue = arc4random() % 255;
    NSInteger aGreenValue = arc4random() % 255;
    NSInteger aBlueValue = arc4random() % 255;
    UIColor *randColor = [UIColor colorWithRed:aRedValue /255.0f green:aGreenValue /255.0f blue:aBlueValue /255.0f alpha:alpha];
    return randColor;
}

/// 屏幕宽高
static inline CGFloat ag_screenWidth(){
    return [UIScreen mainScreen].bounds.size.width;
}

static inline CGFloat ag_screenHeight(){
    return [UIScreen mainScreen].bounds.size.height;
}

/// 判断是否是刘海手机
static inline BOOL ag_isPhoneX(){
    BOOL isBangsScreen = NO;
    if (@available(iOS 11.0, *)) {
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
        isBangsScreen = window.safeAreaInsets.bottom > 0;
    }
    return isBangsScreen;
}

/// NavBar高度
static inline CGFloat ag_navBarHei(){
    return ag_isPhoneX() ? 88 : 64;
}

/// TabBar高度
static inline CGFloat ag_tabBarHei(){
    return ag_isPhoneX() ? 83 : 49;
}

static inline CGFloat ag_statusBarHeight(){
    return ag_isPhoneX() ? 44 : 20;
}

static inline CGFloat ag_safeTopOffset(){
    return ag_isPhoneX() ? 44 : 20;
}

static inline CGFloat ag_safeBottomOffset(){
    return ag_isPhoneX() ? 34 : 0;
}


#endif /* AGCommon_h */
