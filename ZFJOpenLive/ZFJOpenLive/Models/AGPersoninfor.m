//
//  AGPersoninfor.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import "AGPersoninfor.h"
#import "AGLogViewController.h"

@interface AGPersoninfor ()

///
@property (nonatomic,strong) NSMutableArray *logsArray;
///
@property (nonatomic,strong) AGLogViewController *logVC;

@end

@implementation AGPersoninfor

+ (instancetype)sharedInstance{
    static AGPersoninfor *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[AGPersoninfor alloc] init];
    });
    return instance;
}

/// 打印日志
- (void)printLog:(NSString *)log{
    [self.logsArray addObject:log];
    self.logVC.oneLog = log;
}

- (NSMutableArray *)logsArray{
    if(_logsArray == nil){
        _logsArray = [[NSMutableArray alloc] init];
    }
    return _logsArray;
}

- (AGLogViewController *)logVC{
    if(_logVC == nil){
        _logVC = [[AGLogViewController alloc] init];
    }
    return _logVC;
}

@end
