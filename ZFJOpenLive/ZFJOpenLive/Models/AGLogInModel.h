//
//  AGLogInModel.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AGLogInModel : NSObject

@property (nonatomic,  copy) NSString *title;
///
@property (nonatomic,  copy) NSString *value;
///
@property (nonatomic,  copy) NSString *placeholder;
///
@property (nonatomic,assign) UIKeyboardType keyboardType;
/// 可选的
@property (nonatomic,assign) BOOL optional;

@end

NS_ASSUME_NONNULL_END
