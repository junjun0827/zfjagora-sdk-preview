//
//  AGPersoninfor.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import <Foundation/Foundation.h>

#define kAGPersoninfor [AGPersoninfor sharedInstance]

@interface AGPersoninfor : NSObject

+ (AGPersoninfor *)sharedInstance;

///
@property (nonatomic,  copy) NSString *token;
///
@property (nonatomic,  copy) NSString *appId;
///
@property (nonatomic,  copy) NSString *channelId;
///
@property (nonatomic,assign) NSInteger uid;
///
@property (nonatomic,assign) AgoraClientRole agoraClientRole;
/// 开启耳返功能
@property (nonatomic,assign) BOOL enableInEarMonitoring;
/// 开启直播HD
@property (nonatomic,assign) BOOL enableVideoDimensionHD;

/// 打印日志
- (void)printLog:(NSString *)log;

@end
