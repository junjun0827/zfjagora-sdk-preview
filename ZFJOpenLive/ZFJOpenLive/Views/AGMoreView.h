//
//  AGMoreView.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/29.
//

#import "AGBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AGMoreView : AGBaseView

+ (void)showMoreViewWithRtcEngine:(AgoraRtcEngineKit *)agoraKit completion:(void(^)(NSInteger type))completion;

@end

NS_ASSUME_NONNULL_END
