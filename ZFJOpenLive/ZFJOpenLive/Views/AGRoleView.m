//
//  AGRoleView.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "AGRoleView.h"

@interface AGRoleView ()

@property (nonatomic,assign) AgoraClientRole agoraClientRole;
///
@property (nonatomic,strong) UIImageView *leftImgView;
/// 箭头
@property (nonatomic,strong) UIImageView *arrowImgView;
///
@property (nonatomic,strong) UILabel *titleLabel;
///
@property (nonatomic,strong) UILabel *detailLabel;

@end

@implementation AGRoleView

- (instancetype)initWithType:(AgoraClientRole)agoraClientRole{
    if(self == [super init]){
        _agoraClientRole = agoraClientRole;
        [self uiConfig];
    }
    return self;
}

- (void)uiConfig{
    self.backgroundColor = UIColor.whiteColor;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 10;
    
    [self.leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.centerY.equalTo(self);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(130);
    }];
    
    [self.arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-8);
        make.centerY.equalTo(self);
        make.width.mas_equalTo(25);
        make.height.mas_equalTo(25);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftImgView).offset(-14);
        make.left.equalTo(self.leftImgView.mas_right);
        make.height.mas_equalTo(20);
    }];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftImgView).offset(14);
        make.left.equalTo(self.leftImgView.mas_right);
        make.height.mas_equalTo(20);
    }];
    
    if(_agoraClientRole == AgoraClientRoleBroadcaster){
        self.leftImgView.image = [UIImage imageNamed:@"broadcaster"];
        self.titleLabel.text = @"我是主播";
        self.detailLabel.text = @"以主播身份使用App";
    }else{
        self.leftImgView.image = [UIImage imageNamed:@"audience"];
        self.titleLabel.text = @"我是观众";
        self.detailLabel.text = @"以观众身份使用App";
    }
}

- (UILabel *)detailLabel{
    if(_detailLabel == nil){
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.font = [UIFont systemFontOfSize:14];
        _detailLabel.textColor = UIColor.systemGrayColor;
        [self addSubview:_detailLabel];
    }
    return _detailLabel;
}

- (UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:18];
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UIImageView *)arrowImgView{
    if(_arrowImgView == nil){
        _arrowImgView = [[UIImageView alloc] init];
        _arrowImgView.image = [UIImage imageNamed:@"ic_arrow_right"];
        [self addSubview:_arrowImgView];
    }
    return _arrowImgView;
}

- (UIImageView *)leftImgView{
    if(_leftImgView == nil){
        _leftImgView = [[UIImageView alloc] init];
        [self addSubview:_leftImgView];
    }
    return _leftImgView;
}

@end
