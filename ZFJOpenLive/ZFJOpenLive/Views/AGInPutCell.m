//
//  AGInPutCell.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import "AGInPutCell.h"
#import "AGLogInModel.h"

#define KCellFont [UIFont systemFontOfSize:16]

@interface AGInPutCell ()

@property (nonatomic,strong) UILabel *titleLabel;
///
@property (nonatomic,strong) UITextField *textField;
///
@property (nonatomic,strong) UIView *lineView;

@end

@implementation AGInPutCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self uiConfig];
    }
    return self;
}

- (void)uiConfig{
    self.contentView.backgroundColor = UIColor.clearColor;
    self.backgroundColor = UIColor.clearColor;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(- 15);
        make.top.equalTo(self.contentView).offset(15);
        make.height.mas_equalTo(20);
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(- 15);
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textField.mas_bottom).offset(0);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(-1);
        make.height.mas_equalTo(1);
    }];
}

- (void)textField1TextChange:(UITextField *)textField{
    if(_model != nil){
        _model.value = textField.text;
    }
}

- (void)setModel:(AGLogInModel *)model{
    _model = model;
    if(_model){
        if(_model.optional){
            self.titleLabel.text = [NSString stringWithFormat:@"%@ (optional)",ag_stringValue(_model.title)];
        }else{
            self.titleLabel.text = ag_stringValue(_model.title);
        }
        ///
        self.textField.text = ag_stringValue(_model.value);
        ///
        NSMutableDictionary *attrs = [[NSMutableDictionary alloc] init];
        attrs[NSFontAttributeName] = KCellFont;
        attrs[NSForegroundColorAttributeName] = [UIColor ag_colorWithHexStr:@"ffffff" alpha:0.5];
        NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:ag_stringValue(_model.placeholder) attributes:attrs];
        self.textField.attributedPlaceholder = attStr;
        ///
        self.textField.keyboardType = _model.keyboardType;
    }
}

- (UIView *)lineView{
    if(_lineView == nil){
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColor.whiteColor;
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.textColor = UIColor.whiteColor;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UITextField *)textField{
    if(_textField == nil){
        _textField = [[UITextField alloc] init];
        _textField.font = KCellFont;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.textColor = UIColor.whiteColor;
        [_textField addTarget:self action:@selector(textField1TextChange:) forControlEvents:UIControlEventEditingChanged];
        [self.contentView addSubview:_textField];
    }
    return _textField;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
