//
//  AGInPutCell.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import <UIKit/UIKit.h>

@class AGLogInModel;

NS_ASSUME_NONNULL_BEGIN

@interface AGInPutCell : UITableViewCell

@property (nonatomic,strong) AGLogInModel *model;

@end

NS_ASSUME_NONNULL_END
