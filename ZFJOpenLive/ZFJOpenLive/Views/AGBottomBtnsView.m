//
//  AGBottomBtnsView.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "AGBottomBtnsView.h"

/// 按钮之间的间距
#define kAGBottomBtnSpace 10.0
/// 按钮的宽高
#define kAGBottomBtnSize  36.0

@interface AGBottomBtnsView ()

/// 转换摄像头的按钮
@property (nonatomic,strong) UIButton *switchCameraBtn;
/// 打开关闭美颜
@property (nonatomic,strong) UIButton *beautyBtn;
/// 关闭视频推流
@property (nonatomic,strong) UIButton *muteVideoBtn;
/// 关闭声音推流
@property (nonatomic,strong) UIButton *muteAudioBtn;

@end

@implementation AGBottomBtnsView

- (void)uiConfig{
    [self.switchCameraBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kAGBottomBtnSpace);
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(kAGBottomBtnSize);
    }];
    
    [self.beautyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.switchCameraBtn.mas_right).offset(kAGBottomBtnSpace);
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(kAGBottomBtnSize);
    }];
    
    [self.muteAudioBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-kAGBottomBtnSpace);
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(kAGBottomBtnSize);
    }];
    
    [self.muteVideoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.muteAudioBtn.mas_left).offset(-kAGBottomBtnSpace);
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(kAGBottomBtnSize);
    }];
}

/// 底部按钮的点击事件
- (void)bottomBtnsClick:(UIButton *)button{
    if(self.bottomBtnBlock == nil) return;
    if(button == self.switchCameraBtn){
        self.bottomBtnBlock(button, AGSwitchCameraBtnType);
    }else if(button == self.beautyBtn){
        self.bottomBtnBlock(button, AGBeautyBtnType);
    }else if(button == self.muteVideoBtn){
        self.bottomBtnBlock(button, AGMuteVideoBtnType);
    }else if(button == self.muteAudioBtn){
        self.bottomBtnBlock(button, AGMuteAudioBtnType);
    }
    
    button.selected = !button.selected;
}

- (UIButton *)switchCameraBtn{
    if(_switchCameraBtn == nil){
        _switchCameraBtn = [[UIButton alloc] init];
        [_switchCameraBtn setImage:[UIImage imageNamed:@"icon-rotate"] forState:UIControlStateNormal];
        [_switchCameraBtn setImage:[UIImage imageNamed:@"icon-rotate"] forState:UIControlStateSelected];
        [_switchCameraBtn addTarget:self action:@selector(bottomBtnsClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_switchCameraBtn];
    }
    return _switchCameraBtn;
}

- (UIButton *)beautyBtn{
    if(_beautyBtn == nil){
        _beautyBtn = [[UIButton alloc] init];
        [_beautyBtn setImage:[UIImage imageNamed:@"icon-beaty"] forState:UIControlStateNormal];
        [_beautyBtn setImage:[UIImage imageNamed:@"icon-beaty"] forState:UIControlStateSelected];
        [_beautyBtn addTarget:self action:@selector(bottomBtnsClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_beautyBtn];
    }
    return _beautyBtn;
}

- (UIButton *)muteVideoBtn{
    if(_muteVideoBtn == nil){
        _muteVideoBtn = [[UIButton alloc] init];
        [_muteVideoBtn setImage:[UIImage imageNamed:@"icon-camera"] forState:UIControlStateNormal];
        [_muteVideoBtn setImage:[UIImage imageNamed:@"icon-camera off"] forState:UIControlStateSelected];
        [_muteVideoBtn addTarget:self action:@selector(bottomBtnsClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_muteVideoBtn];
    }
    return _muteVideoBtn;
}

- (UIButton *)muteAudioBtn{
    if(_muteAudioBtn == nil){
        _muteAudioBtn = [[UIButton alloc] init];
        [_muteAudioBtn setImage:[UIImage imageNamed:@"icon-microphone"] forState:UIControlStateNormal];
        [_muteAudioBtn setImage:[UIImage imageNamed:@"icon-microphone off"] forState:UIControlStateSelected];
        [_muteAudioBtn addTarget:self action:@selector(bottomBtnsClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_muteAudioBtn];
    }
    return _muteAudioBtn;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
