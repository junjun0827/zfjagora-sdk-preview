//
//  AGConnectionStateView.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/29.
//

#import "AGConnectionStateView.h"

@interface AGConnectionStateView ()

@property (nonatomic,strong) UIView *stateView;
///
@property (nonatomic,strong) UILabel *stateTxtLabel;

@property (nonatomic,strong) UIView *networkTypeStateView;
///
@property (nonatomic,strong) UILabel *networkTypeLabel;

@end

@implementation AGConnectionStateView

- (void)uiConfig{
    [self.stateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(10);
    }];
    
    [self.stateTxtLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.stateView.mas_right).offset(8);
        make.centerY.equalTo(self);
        make.height.mas_equalTo(15);
    }];
    
    [self.networkTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.centerY.equalTo(self);
        make.height.mas_equalTo(15);
    }];
    
    [self.networkTypeStateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.networkTypeLabel.mas_left).offset(-8);
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(10);
    }];
}

- (void)setConnectionStateType:(AgoraConnectionState)connectionStateType{
    self.stateView.hidden = NO;
    self.stateTxtLabel.hidden = NO;
    if(connectionStateType == AgoraConnectionStateDisconnected){
        /// 网络连接断开
        self.stateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#ffcb00"];
        self.stateTxtLabel.text = @"网络连接断开";
    }else if (connectionStateType == AgoraConnectionStateConnecting){
        /// 建立网络连接中
        self.stateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#18C7DB"];
        self.stateTxtLabel.text = @"建立网络连接中...";
    }else if (connectionStateType == AgoraConnectionStateConnected){
        /// 网络已连接
        self.stateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#44C018"];
        self.stateTxtLabel.text = @"网络已连接";
    }else if (connectionStateType == AgoraConnectionStateReconnecting){
        /// 重新建立网络连接中
        self.stateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#18C7DB"];
        self.stateTxtLabel.text = @"重新建立网络连接中...";
    }else if (connectionStateType == AgoraConnectionStateFailed){
        /// 网络连接失败
        self.stateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#ED513F"];
        self.stateTxtLabel.text = @"网络连接失败";
    }else{
        ///
        self.stateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#3ED6FF"];
        self.stateTxtLabel.text = @"未知";
    }
}

- (void)setNetworkType:(AgoraNetworkType)networkType{
    _networkType = networkType;
    self.networkTypeStateView.hidden = NO;
    self.networkTypeLabel.hidden = NO;
    if(networkType == AgoraNetworkTypeUnknown){
        self.networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#F59846"];
        self.networkTypeLabel.text = @"未知";
    }else if(networkType == AgoraNetworkTypeDisconnected){
        self.networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#ED513F"];
        self.networkTypeLabel.text = @"未连接";
    }else if(networkType == AgoraNetworkTypeLAN){
        self.networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#0088FF"];
        self.networkTypeLabel.text = @"LAN";
    }else if(networkType == AgoraNetworkTypeWIFI){
        self.networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#44C018"];
        self.networkTypeLabel.text = @"WIFI";
    }else if(networkType == AgoraNetworkType2G){
        self.networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#ED513F"];
        self.networkTypeLabel.text = @"2G";
    }else if(networkType == AgoraNetworkType3G){
        self.networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#18C7DB"];
        self.networkTypeLabel.text = @"3G";
    }else if(networkType == AgoraNetworkType4G){
        self.networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#18C7DB"];
        self.networkTypeLabel.text = @"4G";
    }
}

- (UILabel *)stateTxtLabel{
    if(_stateTxtLabel == nil){
        _stateTxtLabel = [[UILabel alloc] init];
        _stateTxtLabel.textColor = UIColor.whiteColor;
        _stateTxtLabel.font = [UIFont systemFontOfSize:12];
        _stateTxtLabel.text = @"";
        _stateTxtLabel.hidden = YES;
        [self addSubview:_stateTxtLabel];
    }
    return _stateTxtLabel;
}

- (UIView *)stateView{
    if(_stateView == nil){
        _stateView = [[UIView alloc] init];
        _stateView.layer.masksToBounds = YES;
        _stateView.layer.cornerRadius = 10/2;
        _stateView.hidden = YES;
        [self addSubview:_stateView];
    }
    return _stateView;
}

- (UIView *)networkTypeStateView{
    if(_networkTypeStateView == nil){
        _networkTypeStateView = [[UIView alloc] init];
        _networkTypeStateView.layer.masksToBounds = YES;
        _networkTypeStateView.layer.cornerRadius = 10/2;
        _networkTypeStateView.backgroundColor = [UIColor ag_colorWithHexStr:@"#F59846"];
        _networkTypeStateView.hidden = YES;
        [self addSubview:_networkTypeStateView];
    }
    return _networkTypeStateView;
}

- (UILabel *)networkTypeLabel{
    if(_networkTypeLabel == nil){
        _networkTypeLabel = [[UILabel alloc] init];
        _networkTypeLabel.textColor = UIColor.whiteColor;
        _networkTypeLabel.font = [UIFont systemFontOfSize:12];
        _networkTypeLabel.text = @"未知";
        _networkTypeLabel.hidden = YES;
        [self addSubview:_networkTypeLabel];
    }
    return _networkTypeLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
