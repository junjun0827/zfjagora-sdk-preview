//
//  AGConnectionStateView.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/29.
//

#import "AGBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AGConnectionStateView : AGBaseView

@property (nonatomic,assign) AgoraConnectionState connectionStateType;

/// 本地网络类型发生改变回调。
@property (nonatomic,assign) AgoraNetworkType networkType;

@end

NS_ASSUME_NONNULL_END
