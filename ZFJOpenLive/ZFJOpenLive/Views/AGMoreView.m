//
//  AGMoreView.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/29.
//

#import "AGMoreView.h"
#import "AGMoreViewCell.h"
#import "UIView+AGAddTouchEvent.h"

@interface AGMoreView ()<UITableViewDelegate, UITableViewDataSource, AGMoreViewCellDelegate>

@property (nonatomic, copy) void (^completion)(NSInteger type);
///
@property (nonatomic,strong) UIView *contentView;
///
@property (nonatomic,strong) UILabel *titleLab;
///
@property (nonatomic,strong) UITableView *tableView;
///
@property (nonatomic,strong) NSArray<NSDictionary *> *titleArray;
///
@property (nonatomic,strong) AgoraRtcEngineKit *agoraKit;

@end

@implementation AGMoreView

+ (void)showMoreViewWithRtcEngine:(AgoraRtcEngineKit *)agoraKit completion:(void(^)(NSInteger type))completion{
    AGMoreView *moreView = [[AGMoreView alloc] initWithAgoraKit:agoraKit];
    moreView.frame = CGRectMake(0, 0, ag_screenWidth(), ag_screenHeight());
    moreView.backgroundColor = [UIColor ag_colorWithHexStr:@"000000" alpha:0.4];
    [[JKRouter sharedRouter].topVC.view addSubview:moreView];
    [moreView showMoreView];
}

- (instancetype)initWithAgoraKit:(AgoraRtcEngineKit *)agoraKit{
    if(self == [super init]){
        _agoraKit = agoraKit;
        [self uiConfig];
    }
    return self;
}

- (void)uiConfig{
    self.contentView.frame = CGRectMake(ag_screenWidth(), 0, ag_screenWidth()/2, ag_screenHeight());
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(ag_safeTopOffset());
        make.height.mas_equalTo(44);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.titleLab.mas_bottom).offset(8);
        make.bottom.equalTo(self.contentView).offset(-ag_safeBottomOffset());
    }];
    self.tableView.layer.borderWidth = 0.5;
    self.tableView.layer.borderColor = UIColor.redColor.CGColor;
    
    WEAKBLOCK;
    self.touchUpInsideBlock = ^(UIView *sender) {
        [weakSelf hideMoreView];
    };
}

- (void)showMoreView{
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.frame = CGRectMake(ag_screenWidth()/2, 0, ag_screenWidth()/2, ag_screenHeight());
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideMoreView{
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
        self.contentView.frame = CGRectMake(ag_screenWidth(), 0, ag_screenWidth()/2, ag_screenHeight());
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AGMoreViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AGMoreViewCell.className];
    cell.delegate = self;
    cell.dict = [self.titleArray objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

#pragma mark - AGMoreViewCellDelegate
- (void)moreViewCellValueChanged:(UISwitch *)switchBtn dict:(NSDictionary *)dict{
    NSInteger index = [self.titleArray indexOfObject:dict];
    if([dict[@"key"] isEqualToString:@"enableInEarMonitoring"]){
        [self.agoraKit enableInEarMonitoring:switchBtn.on];
        kAGPersoninfor.enableInEarMonitoring = switchBtn.on;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }else if ([dict[@"key"] isEqualToString:@"enableVideoDimensionHD"]){
        AgoraVideoEncoderConfiguration *configuration = [[AgoraVideoEncoderConfiguration alloc] init];
        if(switchBtn.on){
            [self.agoraKit setParameters:@"{\"che.video.mobile_1080p\":true}"];
            configuration.dimensions = CGSizeMake(1280, 720);
        }else{
            [self.agoraKit setParameters:@"{\"che.video.mobile_1080p\":false}"];
            configuration.dimensions = CGSizeMake(ag_screenWidth(), ag_screenHeight());
        }
        [self.agoraKit setVideoEncoderConfiguration:configuration];
        kAGPersoninfor.enableVideoDimensionHD = switchBtn.on;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)dealloc{
    NSLog(@"AGMoreView");
}

- (UIView *)contentView{
    if(_contentView == nil){
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = UIColor.whiteColor;
        _contentView.frame = CGRectMake(ag_screenWidth(), 0, ag_screenWidth()/2, ag_screenHeight());
        [self addSubview:_contentView];
    }
    return _contentView;
}

- (UILabel *)titleLab{
    if(_titleLab == nil){
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16];
        _titleLab.text = @"设置";
        [self.contentView addSubview:_titleLab];
    }
    return _titleLab;
}

- (UITableView *)tableView{
    if(_tableView == nil){
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ag_screenWidth()/2, ag_screenHeight()) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.backgroundColor = UIColor.clearColor;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[AGMoreViewCell class] forCellReuseIdentifier:AGMoreViewCell.className];
        [self.contentView addSubview:_tableView];
    }
    return _tableView;
}

- (NSArray<NSDictionary *> *)titleArray{
    if(_titleArray == nil){
        /// 2660
        _titleArray = @[@{@"title":@"开启耳返功能", @"key":@"enableInEarMonitoring"},
                        @{@"title":@"开启HD", @"key":@"enableVideoDimensionHD"}
        ];
    }
    return _titleArray;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
