//
//  AGBottomBtnsView.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "AGBaseView.h"

typedef NS_ENUM(NSUInteger, AGBottomBtnType) {
    AGSwitchCameraBtnType  = 0,   /// 转换摄像头的按钮
    AGBeautyBtnType        = 1,   /// 打开关闭美颜
    AGMuteVideoBtnType     = 2,   /// 关闭视频推流
    AGMuteAudioBtnType     = 3,   /// 关闭声音推流
};

NS_ASSUME_NONNULL_BEGIN

@interface AGBottomBtnsView : AGBaseView

/// 底部按钮的点击事件
@property (nonatomic, copy) void (^bottomBtnBlock)(UIButton *button, AGBottomBtnType type);

@end

NS_ASSUME_NONNULL_END
