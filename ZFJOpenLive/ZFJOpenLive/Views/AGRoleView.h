//
//  AGRoleView.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AGRoleView : UIView

- (instancetype)initWithType:(AgoraClientRole)agoraClientRole;
///
@property (nonatomic,assign,readonly) AgoraClientRole agoraClientRole;

@end

NS_ASSUME_NONNULL_END
