//
//  AGMoreViewCell.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/29.
//

#import "AGMoreViewCell.h"

@interface AGMoreViewCell ()

@property (nonatomic,strong) UILabel *titleLabel;
///
@property (nonatomic,strong) UISwitch *switchBtn;

@end

@implementation AGMoreViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self uiConfig];
    }
    return self;
}

- (void)uiConfig{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView);
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.switchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-4);
        make.centerY.equalTo(self.contentView);
    }];
}

- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
    if(_dict == nil){
        return;
    }
    self.titleLabel.text = _dict[@"title"];
    if([_dict[@"key"] isEqualToString:@"enableInEarMonitoring"]){
        self.switchBtn.on = kAGPersoninfor.enableInEarMonitoring;
    }else if([_dict[@"key"] isEqualToString:@"enableVideoDimensionHD"]){
        self.switchBtn.on = kAGPersoninfor.enableVideoDimensionHD;
    }
}

- (void)switchBtnClick:(UISwitch *)switchBtn{
    if(self.delegate && [self.delegate respondsToSelector:@selector(moreViewCellValueChanged:dict:)] && _dict != nil){
        [self.delegate moreViewCellValueChanged:switchBtn dict:_dict];
    }
}

- (UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UISwitch *)switchBtn{
    if(_switchBtn == nil){
        _switchBtn = [[UISwitch alloc] init];
        [_switchBtn addTarget:self action:@selector(switchBtnClick:) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:_switchBtn];
    }
    return _switchBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
