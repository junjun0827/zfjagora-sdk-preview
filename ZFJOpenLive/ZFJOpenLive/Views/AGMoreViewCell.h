//
//  AGMoreViewCell.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AGMoreViewCellDelegate <NSObject>

- (void)moreViewCellValueChanged:(UISwitch *)switchBtn dict:(NSDictionary *)dict;

@end

@interface AGMoreViewCell : UITableViewCell

@property (nonatomic,strong) NSDictionary *dict;
///
@property (nonatomic,  weak) id<AGMoreViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
