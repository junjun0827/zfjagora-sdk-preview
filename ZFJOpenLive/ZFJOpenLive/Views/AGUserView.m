//
//  AGUserView.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "AGUserView.h"

@interface AGUserView ()

///
@property (nonatomic,strong) UIImageView *avatarView;
///
@property (nonatomic,strong) UIImageView *addImgView;
///
@property (nonatomic,strong) UILabel *usernameLabel;
///
@property (nonatomic,strong) UILabel *detailLabel;

@end

@implementation AGUserView

- (void)uiConfig{
    [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(2);
        make.width.height.mas_equalTo(40);
    }];
    
    [self.addImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(- 8);
        make.width.height.mas_equalTo(30);
        make.centerY.equalTo(self);
    }];
    
    [self.usernameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avatarView.mas_top);
        make.left.equalTo(self.avatarView.mas_right).offset(8);
        make.right.equalTo(self.addImgView.mas_left).offset(-8);
        make.height.mas_equalTo(20);
    }];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.usernameLabel.mas_bottom);
        make.left.equalTo(self.avatarView.mas_right).offset(8);
        make.right.equalTo(self.addImgView.mas_left).offset(-8);
        make.height.mas_equalTo(20);
    }];
}

- (UILabel *)detailLabel{
    if(_detailLabel == nil){
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.font = [UIFont systemFontOfSize:10];
        _detailLabel.textColor = [UIColor ag_colorWithHexStr:@"#ffffff" alpha:0.7];
        _detailLabel.text = @"Shakes";
        [self addSubview:_detailLabel];
    }
    return _detailLabel;
}

- (UILabel *)usernameLabel{
    if(_usernameLabel == nil){
        _usernameLabel = [[UILabel alloc] init];
        _usernameLabel.font = [UIFont systemFontOfSize:12];
        _usernameLabel.textColor = UIColor.whiteColor;
        _usernameLabel.text = @"Myroom";
        [self addSubview:_usernameLabel];
    }
    return _usernameLabel;
}

- (UIImageView *)addImgView{
    if(_addImgView == nil){
        _addImgView = [[UIImageView alloc] init];
        _addImgView.image = [UIImage imageNamed:@"icon-add"];
        _addImgView.layer.masksToBounds = YES;
        _addImgView.layer.cornerRadius = 22;
        [self addSubview:_addImgView];
    }
    return _addImgView;
}

- (UIImageView *)avatarView{
    if(_avatarView == nil){
        _avatarView = [[UIImageView alloc] init];
        _avatarView.image = [UIImage imageNamed:@"head"];
        _avatarView.layer.masksToBounds = YES;
        _avatarView.layer.cornerRadius = 22;
        [self addSubview:_avatarView];
    }
    return _avatarView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
