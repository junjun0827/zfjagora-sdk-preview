//
//  AppDelegate.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/9.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow * window;

@end

