//
//  AGLogViewController.h
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import "AGBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AGLogViewController : AGBaseViewController

@property (nonatomic,  copy) NSString *oneLog;

@end

NS_ASSUME_NONNULL_END
