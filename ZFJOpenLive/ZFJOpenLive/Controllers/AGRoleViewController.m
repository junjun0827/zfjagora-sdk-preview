//
//  AGRoleViewController.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/10.
//

#import "AGRoleViewController.h"
#import "AGRoleView.h"

@interface AGRoleViewController ()

@property (nonatomic,strong) AGRoleView *anchorView;
///
@property (nonatomic,strong) AGRoleView *audienceView;

@end

@implementation AGRoleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self uiConfig];
}

- (void)uiConfig{
    self.view.backgroundColor = UIColor.whiteColor;
    
    UIView *spaceLine = [[UIView alloc] init];
    [self.view addSubview:spaceLine];
    [spaceLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(ag_screenWidth());
        make.height.mas_equalTo(0.5);
        make.center.equalTo(self.view);
    }];
    
    [self.anchorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(30);
        make.right.equalTo(self.view).offset(-30);
        make.bottom.equalTo(spaceLine.mas_top).offset(-10);
        make.height.mas_equalTo(130);
    }];
    
    [self.audienceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(30);
        make.right.equalTo(self.view).offset(-30);
        make.top.equalTo(spaceLine.mas_top).offset(10);
        make.height.mas_equalTo(130);
    }];
    
}

- (void)roleViewTouchUpInside:(AGRoleView *)roleView{
    [AGPersoninfor sharedInstance].agoraClientRole = roleView.agoraClientRole;
    [JKRouter open:@"AGLiveRoomViewController"];
}

- (AGRoleView *)anchorView{
    if(_anchorView == nil){
        WEAKBLOCK;
        _anchorView = [[AGRoleView alloc] initWithType:AgoraClientRoleBroadcaster];
        _anchorView.touchUpInsideBlock = ^(UIView *sender) {
            [weakSelf roleViewTouchUpInside:(AGRoleView *)sender];
        };
        [self.view addSubview:_anchorView];
    }
    return _anchorView;
}

- (AGRoleView *)audienceView{
    if(_audienceView == nil){
        WEAKBLOCK;
        _audienceView = [[AGRoleView alloc] initWithType:AgoraClientRoleAudience];
        _audienceView.touchUpInsideBlock = ^(UIView *sender) {
            [weakSelf roleViewTouchUpInside:(AGRoleView *)sender];
        };
        [self.view addSubview:_audienceView];
    }
    return _audienceView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
