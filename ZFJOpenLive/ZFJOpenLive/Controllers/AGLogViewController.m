//
//  AGLogViewController.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/23.
//

#import "AGLogViewController.h"

@interface AGLogViewController ()

@property (nonatomic,strong) UITextView *textView;

@end

@implementation AGLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self uiConfig];
}

- (void)uiConfig{
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(self.view);
    }];
}

- (void)setOneLog:(NSString *)oneLog{
    _oneLog = oneLog;
    if(!ag_emptyString(_oneLog)){
        NSString *logs = self.textView.text;
        if(ag_emptyString(logs)){
            self.textView.text = [logs stringByAppendingString:oneLog];
        }else{
            self.textView.text = [logs stringByAppendingString:[NSString stringWithFormat:@"\n%@",oneLog]];
        }
    }
}

- (UITextView *)textView{
    if(_textView == nil){
        _textView = [[UITextView alloc] init];
        _textView.backgroundColor = UIColor.blackColor;
        _textView.textColor = UIColor.whiteColor;
        _textView.font = [UIFont systemFontOfSize:16];
        [self.view addSubview:_textView];
    }
    return _textView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
