//
//  ViewController.m
//  ZFJOpenLive
//
//  Created by ZhangFujie on 2021/12/9.
//

#import "ViewController.h"
#import "AGInPutCell.h"
#import "AGLogInModel.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
/// 开始直播
@property (nonatomic,strong) UIButton *startLiveBtn;
/// 数据源数组
@property (nonatomic,strong) NSMutableArray<AGLogInModel *> *dataArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self dataConfig];
    [self uiConfig];
}

- (void)dataConfig{
    WEAKBLOCK;
    NSArray<NSString *> *titleArray = @[@"token", @"appId", @"channelId", @"uid"];
    [titleArray enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        AGLogInModel *model = [[AGLogInModel alloc] init];
        model.title = obj;
        model.placeholder = [NSString stringWithFormat:@"Please input a %@", obj];
        if(idx == 3){
            model.keyboardType = UIKeyboardTypeNumberPad;
            model.value = @"0";
        }else{
            model.keyboardType = UIKeyboardTypeDefault;
        }
        if(idx == 0 || idx == 3){
            model.optional = YES;
        }else{
            model.optional = NO;
        }
        NSString *value = [[NSUserDefaults standardUserDefaults] valueForKey:obj];
        if(!ag_emptyString(value)){
            model.value = value;
        }
        [weakSelf.dataArray addObject:model];
    }];
}

- (void)uiConfig{
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(self.view);
    }];
    
    [self.startLiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(- ag_safeBottomOffset() - 20);
        make.left.equalTo(self.view).offset(50);
        make.right.equalTo(self.view).offset(-50);
        make.height.mas_equalTo(50);
    }];
}

/// 开始直播
- (void)startLiveBtnClick{
    __block BOOL isNext = YES;
    [self.dataArray enumerateObjectsUsingBlock:^(AGLogInModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(ag_emptyString(obj.value) && !obj.optional){
            [MBProgressHUD showError:ag_stringValue(obj.placeholder)];
            *stop = YES;
            isNext = NO;
            return;
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:ag_stringValue(obj.value) forKey:ag_stringValue(obj.title)];
            [[NSUserDefaults standardUserDefaults] synchronize];
            /// 存单利
            if([obj.title isEqualToString:@"token"]){
                [AGPersoninfor sharedInstance].token = ag_stringValue(obj.value);
            }else if([obj.title isEqualToString:@"appId"]){
                [AGPersoninfor sharedInstance].appId = ag_stringValue(obj.value);
            }else if([obj.title isEqualToString:@"channelId"]){
                [AGPersoninfor sharedInstance].channelId = ag_stringValue(obj.value);
            }else if([obj.title isEqualToString:@"uid"]){
                [AGPersoninfor sharedInstance].uid = [ag_stringValue(obj.value) integerValue];
            }
        }
    }];
    if(isNext){
        [JKRouter open:@"AGRoleViewController"];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 86;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AGInPutCell *cell = [tableView dequeueReusableCellWithIdentifier:AGInPutCell.className];
    cell.model = [self.dataArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

- (UIButton *)startLiveBtn{
    if(_startLiveBtn == nil){
        _startLiveBtn = [[UIButton alloc] init];
        [_startLiveBtn setTitle:@"开始直播" forState:UIControlStateNormal];
        [_startLiveBtn setTitleColor:UIColor.systemBlueColor forState:UIControlStateNormal];
        _startLiveBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        _startLiveBtn.backgroundColor = UIColor.whiteColor;
        _startLiveBtn.layer.masksToBounds = YES;
        _startLiveBtn.layer.cornerRadius = 25;
        [_startLiveBtn addTarget:self action:@selector(startLiveBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_startLiveBtn];
    }
    return _startLiveBtn;
}

- (UITableView *)tableView{
    if(_tableView == nil){
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ag_screenWidth(), ag_screenHeight()) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.backgroundColor = UIColor.clearColor;
        _tableView.estimatedRowHeight = 50;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[AGInPutCell class] forCellReuseIdentifier:AGInPutCell.className];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (NSMutableArray<AGLogInModel *> *)dataArray{
    if(_dataArray == nil){
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

@end
